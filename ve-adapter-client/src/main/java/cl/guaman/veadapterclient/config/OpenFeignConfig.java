package cl.guaman.veadapterclient.config;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = {"cl.guaman.veadapterclient"})
public class OpenFeignConfig {
}
