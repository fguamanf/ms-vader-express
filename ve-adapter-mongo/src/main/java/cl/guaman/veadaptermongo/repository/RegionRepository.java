package cl.guaman.veadaptermongo.repository;

import cl.guaman.veadaptermongo.document.RegionDocument;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface RegionRepository extends ReactiveCrudRepository<RegionDocument, Integer> {

}