package cl.guaman.veadaptermongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VeAdapterMongoApplication {

    public static void main(String[] args) {
        SpringApplication.run(VeAdapterMongoApplication.class, args);
    }

}
