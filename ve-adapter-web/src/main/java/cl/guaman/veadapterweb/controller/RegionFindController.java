package cl.guaman.veadapterweb.controller;


import cl.guaman.veapplication.domain.Region;
import cl.guaman.veapplication.domain.port.find.RegionFindPort;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping(path = "/regions", produces = MediaType.APPLICATION_JSON_VALUE)
public class RegionFindController {

    private final RegionFindPort regionFindPort;

    @GetMapping(path = "")
    public ResponseEntity findAll() {
        List<Region> regions = regionFindPort.findAll();
        regions.isEmpty();
        if (regions != null || regions.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(regions);
    }
}
