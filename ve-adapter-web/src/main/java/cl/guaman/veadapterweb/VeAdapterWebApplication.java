package cl.guaman.veadapterweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VeAdapterWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(VeAdapterWebApplication.class, args);
    }

}
