package cl.guaman.veadapterweb.controller

import cl.guaman.veapplication.domain.port.find.RegionFindPort
import org.springframework.http.ResponseEntity
import spock.lang.Specification

class RegionFindControllerSpec extends Specification {

    RegionFindController regionFindController
    RegionFindPort regionFindPort = Mock()

    def setup() {
        regionFindController = new RegionFindController(regionFindPort)
    }

    def "find all regions"() {
        given:
        regionFindPort.findAll() >> Collections.emptyList()
        when:
        ResponseEntity responseEntity = regionFindController.findAll()
        then:
        responseEntity.statusCode.is2xxSuccessful()
    }
}
