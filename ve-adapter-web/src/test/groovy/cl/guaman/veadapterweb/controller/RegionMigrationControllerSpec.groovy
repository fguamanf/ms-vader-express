package cl.guaman.veadapterweb.controller

import cl.guaman.veapplication.domain.usecase.migration.RegionMigrationUseCase
import org.springframework.http.ResponseEntity
import spock.lang.Specification

class RegionMigrationControllerSpec extends Specification {

    RegionMigrationController regionMigrationController
    RegionMigrationUseCase regionMigrationUseCase = Mock()


    def setup() {
        regionMigrationController = new RegionMigrationController(regionMigrationUseCase)
    }

    def "migration region's from api to db"() {
        given:
        regionMigrationUseCase.fromApiToDb() >> {}
        when:
        ResponseEntity responseEntity = regionMigrationController.fromApiToDb()
        then:
        responseEntity.statusCode.is2xxSuccessful()
    }
}
